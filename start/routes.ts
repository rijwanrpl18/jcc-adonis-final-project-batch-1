/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| This file is dedicated for defining HTTP routes. A single file is enough
| for majority of projects, however you can define routes in different
| files and just make sure to import them inside this file. For example
|
| Define routes in following two files
| ├── start/routes/cart.ts
| ├── start/routes/customer.ts
|
| and then import them inside `start/routes.ts` as follows
|
| import './routes/cart'
| import './routes/customer'
|
*/

import Route from '@ioc:Adonis/Core/Route'

Route.get('/', async () => {
  return { hello: 'world' }
})


Route.group(() => {
	Route.post('/register', 'AuthController.register').as('auth.register')
	Route.post('/login', 'AuthController.login').as('auth.login')
	Route.post('/otp-confirmation', 'AuthController.otp_verification').as('auth.otp')

	Route.resource('venues', 'VenuesController').apiOnly().middleware({
		'*': ['auth', 'acl:owner', 'verify']
	})
	Route.post('venues/:id/bookings', 'VenuesController.booking').middleware(['auth', 'acl:user']).as('venues.bookings')
	Route.resource('venues.fields', 'FieldsController').apiOnly().middleware({
		'*': ['auth', 'acl:owner', 'verify']
	})
	Route.resource('fields.bookings', 'BookingsController').apiOnly().middleware({
		'*': ['auth', 'verify'],
		'create': ['acl:user', 'verify']
	})
	Route.get('/bookings', 'BookingsController.index').middleware(['auth', 'verify', 'acl:user,owner']).as('booking.index')
	Route.get('/bookings/:id', 'BookingsController.show').middleware(['auth', 'verify', 'acl:user,owner']).as('booking.show')
	Route.put('/bookings/:id/join', 'BookingsController.join').middleware(['auth', 'verify', 'acl:user']).as('booking.join')
	Route.put('/bookings/:id/unjoin', 'BookingsController.unjoin').middleware(['auth', 'verify', 'acl:user']).as('booking.unjoin')
	Route.get('/schedules', 'BookingsController.schedules').middleware(['auth', 'verify', 'acl:user']).as('booking.schedule')
}).prefix('/api/v1')


// Route.group(() => {
// 	Route.get('/venues', 'VenuesController.index').as('venues.index')
// 	Route.post('/venues', 'VenuesController.store').as('venues.store')
// 	Route.get('/venues/:id', 'VenuesController.show').as('venues.show')
// 	Route.put('/venues/:id', 'VenuesController.update').as('venues.update')
// 	Route.delete('/venues/:id', 'VenuesController.destroy').as('venues.destroy')
// }).middleware('auth')

// // Route.resource('fields', 'FieldsController').except(['create', 'edit'])
// Route.resource('venues.fields', 'FieldsController').apiOnly().middleware({
//   '*': ['auth']
// })

// Route.post('/bookings', 'BookingsController.store').as('bookings.store').middleware('auth')
