import { DateTime } from 'luxon'
import Hash from '@ioc:Adonis/Core/Hash'
import {
	column,
	beforeSave,
	BaseModel,
	hasMany,
	HasMany,
} from '@ioc:Adonis/Lucid/Orm'
import Booking from './Booking'

/** 
*  @swagger
*  definitions:
*    Register:
*      type: object
*      properties:
*        id:
*          type: uint
*        name:
*          type: string
*        email:
*          type: string
*        password:
*          type: string
*      required:
*        - name
*        - email
*        - password
*/

/** 
*  @swagger
*  definitions:
*    Login:
*      type: object
*      properties:
*        email:
*          type: string
*        password:
*          type: string
*      required:
*        - email
*        - password
*/

/** 
*  @swagger
*  definitions:
*    OTP:
*      type: object
*      properties:
*        email:
*          type: string
*        otp_code:
*          type: number
*      required:
*        - otp_code
*        - email
*/

export default class User extends BaseModel {
	@column({ isPrimary: true })
	public id: number

	@column()
	public name: string

	@column()
	public email: string

	@column()
	public role: string

	@column({ serializeAs: null })
	public password: string

	@column()
	public isVerified: boolean

	@column()
	public rememberMeToken?: string

	@column.dateTime({ autoCreate: true })
	public createdAt: DateTime

	@column.dateTime({ autoCreate: true, autoUpdate: true })
	public updatedAt: DateTime

	@beforeSave()
	public static async hashPassword (user: User) {
		if (user.$dirty.password) {
			user.password = await Hash.make(user.password)
		}
	}

	@hasMany(() => Booking)
	public myBookings: HasMany<typeof Booking>
}
