import { DateTime } from 'luxon'
import { 
	BaseModel, 
	column,
	HasMany,
	hasMany
} from '@ioc:Adonis/Lucid/Orm'
import Field from './Field'

/** 
*  @swagger
*  definitions:
*    Venue:
*      type: object
*      properties:
*        name:
*          type: string
*        address:
*          type: string
*        phone:
*          type: string
*      required:
*        - name
*/

export default class Venue extends BaseModel {
	@column({ isPrimary: true })
	public id: number

	@column.dateTime({ autoCreate: true })
	public createdAt: DateTime

	@column.dateTime({ autoCreate: true, autoUpdate: true })
	public updatedAt: DateTime

	@column()
	public name: string

	@column()
	public address: string

	@column()
	public phone: string

	@hasMany(() => Field)
	public fields: HasMany<typeof Field>
}
