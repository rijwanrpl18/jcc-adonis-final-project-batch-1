import { DateTime } from 'luxon'
import { 
	BaseModel, 
	BelongsTo, 
	belongsTo, 
	column, 
	ManyToMany, 
	manyToMany 
} from '@ioc:Adonis/Lucid/Orm'
import Field from './Field'
import User from './User'

/** 
*  @swagger
*  definitions:
*    Booking:
*      type: object
*      properties:
*        field_id:
*          type: number
*        title:
*          type: string
*        play_date_start:
*          type: datetime
*        play_date_end:
*          type: datetime
*      required:
*        - field_id
*        - title
*        - play_date_start
*        - play_date_end
*/

export default class Booking extends BaseModel {
    @column({ isPrimary: true })
    public id: number

	@column()
	public title: string

	@column.dateTime()
	public playDateStart: DateTime

	@column.dateTime()
	public playDateEnd: DateTime

	@column()
	public userId: number

	@column()
	public fieldId: number

    @column.dateTime({ autoCreate: true })
    public createdAt: DateTime

    @column.dateTime({ autoCreate: true, autoUpdate: true })
    public updatedAt: DateTime

	@belongsTo(() => Field)
	public field: BelongsTo<typeof Field>

	@belongsTo(() => User)
	public user: BelongsTo<typeof User>

	@manyToMany(() => User, {
		pivotTable: 'schedules'
	})
	public players: ManyToMany<typeof User>
}
