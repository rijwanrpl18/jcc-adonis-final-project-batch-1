import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Booking from 'App/Models/Booking'
import Field from 'App/Models/Field'
import CreateBookingValidator from 'App/Validators/CreateBookingValidator'

export default class BookingsController {
    /**
     * @swagger
     * /api/v1/bookings:
     *   get:
     *     security:
     *       - bearerAuth: []
     *     tags:
     *       - Bookings
     *     summary: List Data Booking
     *     responses:
     *       200:
     *         description: Sukses
     *       401:
     *         description: Unauthorized
     */
    public async index ({ response, request }: HttpContextContract){
        // ORM
        if(request.qs().name){
            let name = request.qs().name
            let data = await Booking.findBy('name', name)
            return response.status(200).json({
                status: 'Sukses',
                data: data
            })
        }
        
        let data = await Booking.query()
        
        response.status(200).json({
            status: 'Sukses',
            data: data
        })
    }


    public async store ({ request, response, params, auth }: HttpContextContract) {
        try {
            const field = await Field.findByOrFail('id', params.field_id)
            const payload = await request.validate(CreateBookingValidator)
            const user = auth.user!
            const booking = new Booking()
            booking.title = payload.title
            booking.playDateStart = payload.play_date_start
            booking.playDateEnd = payload.play_date_end
            booking.related('field').associate(field)

            user.related('myBookings').save(booking)

            return response.created({
                status: 'Created',
                data: booking
            })
        } catch (error) {
            response.badRequest({
                errors: error.message
            }) 
        }
    }

    /**
     * @swagger
     * /api/v1/bookings/{id}:
     *   get:
     *     security:
     *       - bearerAuth: []
     *     tags:
     *       - Bookings
     *     summary: Get Data Booking by ID
     *     parameters: 
     *       - in: path
     *         name: id
     *         required: true
     *     responses:
     *       200:
     *         description: Berhasil get data booking by id
     *       404:
     *         description: Data tidak ditemukan
     *       401:
     *         description: Unauthorized
     */
    public async show({response, params}: HttpContextContract) {
        const booking = await Booking
            .query()
            .where('id', params.id)
            .preload('players', (userQuery) => {
                userQuery.select(['id', 'name', 'email'])
            })
            .withCount('players')
            .firstOrFail()
        let rawData = JSON.stringify(booking)
        let data = {
            ...JSON.parse(rawData),
            players_count: booking.$extras.players_count
        }
        return response.ok({
            status: 'Success',
            data: data
        })
    }

    /**
     * @swagger
     * /api/v1/bookings/{id}/join:
     *   put:
     *     security:
     *       - bearerAuth: []
     *     tags:
     *       - Bookings
     *     summary: Join booking
     *     parameters: 
     *       - in: path
     *         name: id
     *         required: true
     *     responses:
     *       200:
     *         description: Join successfully
     *       404:
     *         description: Data tidak ditemukan
     *       401:
     *         description: Unauthorized
     */
    public async join({response, params, auth}: HttpContextContract){
        const booking = await Booking.findOrFail(params.id)
        let user = auth.user!
        await booking.related('players').attach([user.id])

        return response.ok({
            status: 'Sukses',
            data: 'Join successfully'
        })
    }

    /**
     * @swagger
     * /api/v1/bookings/{id}/unjoin:
     *   put:
     *     security:
     *       - bearerAuth: []
     *     tags:
     *       - Bookings
     *     summary: Unjoin booking
     *     parameters: 
     *       - in: path
     *         name: id
     *         required: true
     *     responses:
     *       200:
     *         description: Unjoin successfully
     *       404:
     *         description: Data tidak ditemukan
     *       401:
     *         description: Unauthorized
     */
    public async unjoin({params, auth, response}: HttpContextContract){
        const booking = await Booking.findOrFail(params.id)
        const user = auth.user!
        await booking.related('players').detach([user.id])
        return response.ok({
            status: 'Sukses',
            data: 'Unjoin successfully'
        })
    }

    /**
     * @swagger
     * /api/v1/schedules:
     *   get:
     *     security:
     *       - bearerAuth: []
     *     tags:
     *       - Bookings
     *     summary: List Booking User
     *     responses:
     *       200:
     *         description: Sukses
     *       401:
     *         description: Unauthorized
     */
    public async schedules({response, auth}: HttpContextContract){
        const user = auth.user!
        const booking = await Booking.query().where('user_id', user.id)
        return response.ok({
            status: 'Sukses',
            data: booking
        })
    }
}
