import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Booking from 'App/Models/Booking'
import Field from 'App/Models/Field'
// import Database from '@ioc:Adonis/Lucid/Database'
import Venue from 'App/Models/Venue'
import CreateBookingValidator from 'App/Validators/CreateBookingValidator'
import CreateVenueValidator from 'App/Validators/CreateVenueValidator'
import UpdateVenueValidator from 'App/Validators/UpdateVenueValidator'

export default class VenuesController {
    /**
     * @swagger
     * /api/v1/venues:
     *   get:
     *     security:
     *       - bearerAuth: []
     *     tags:
     *       - Venues
     *     summary: List Data Venue
     *     responses:
     *       200:
     *         description: Sukses
     *       401:
     *         description: Unauthorized
     */
    public async index ({ response, request }: HttpContextContract){
        // ORM
        if(request.qs().name){
            let name = request.qs().name
            let data = await Venue.findBy('name', name)
            return response.status(200).json({
                status: 'Sukses',
                data: data
            })
        }
        
        let data = await Venue.query().preload('fields')
        
        response.status(200).json({
            status: 'Sukses',
            data: data
        })
    }

    /**
     * @swagger
     * /api/v1/venues:
     *   post:
     *     security:
     *       - bearerAuth: []
     *     tags:
     *       - Venues
     *     summary: Store Data Venue
     *     requestBody:
     *       required: true
     *       content:
     *          application/x-www-form-urlencoded:
     *              schema:
     *                  $ref: '#definitions/Venue'
     *          application/json:
     *              schema:
     *                  $ref: '#definitions/Venue'
     *     responses:
     *       201:
     *         description: Sukses
     *       400:
     *         description: Bad Request
     *       401:
     *         description: Unauthorized
     *       422:
     *          description: Unprocessable Entity
     */
    public async store ({ request, response }: HttpContextContract) {
        try {
            let venue = await request.validate(CreateVenueValidator)
            let data = await Venue.create(venue)
            response.created({
                status: 'Sukses',
                data: data
            })
        } catch (error) {
            response.badRequest({
                errors: error.messages
            }) 
        }
    }

    /**
     * @swagger
     * /api/v1/venues/{id}:
     *   get:
     *     security:
     *       - bearerAuth: []
     *     tags:
     *       - Venues
     *     summary: Get Data Venue by ID
     *     parameters: 
     *       - in: path
     *         name: id
     *         required: true
     *     responses:
     *       200:
     *         description: Berhasil get data venue by id
     *       404:
     *         description: Data tidak ditemukan
     *       401:
     *         description: Unauthorized
     */
    public async show ({ response, params }: HttpContextContract){
        let data = await Venue.query().where('id', params.id).preload('fields').firstOrFail()
        response.status(200).json({
            status: 'Berhasil get data venue by id',
            data: data
        })
    }

    /**
     * @swagger
     * /api/v1/venues/{id}/bookings:
     *   post:
     *     security:
     *       - bearerAuth: []
     *     tags:
     *       - Venues
     *     summary: Membuat jadwal booking
     *     parameters: 
     *       - in: path
     *         name: id
     *         required: true
     *     requestBody:
     *       required: true
     *       content:
     *          application/x-www-form-urlencoded:
     *              schema:
     *                  $ref: '#definitions/Booking'
     *          application/json:
     *              schema:
     *                  $ref: '#definitions/Booking'
     *     responses:
     *       201:
     *         description: Sukses
     *       400:
     *         description: Bad Request
     *       401:
     *         description: Unauthorized
     *       404:
     *         description: Data tidak ditemukan
     *       422:
     *          description: Unprocessable Entity
     */
    public async booking ({ request, response, auth }: HttpContextContract) {
        try {
            const field_id = request.input('field_id')
            const field = await Field.findByOrFail('id', field_id)
            const payload = await request.validate(CreateBookingValidator)
            const user = auth.user!
            const booking = new Booking()
            booking.title = payload.title
            booking.playDateStart = payload.play_date_start
            booking.playDateEnd = payload.play_date_end
            booking.related('field').associate(field)

            user.related('myBookings').save(booking)

            return response.created({
                status: 'Created',
                data: booking
            })
        } catch (error) {
            response.badRequest({
                errors: error.message
            }) 
        }
    }

    /**
     * @swagger
     * /api/v1/venues/{id}:
     *   put:
     *     security:
     *       - bearerAuth: []
     *     tags:
     *       - Venues
     *     summary: Update Data Venue
     *     parameters: 
     *       - in: path
     *         name: id
     *         required: true
     *     requestBody:
     *       required: true
     *       content:
     *          application/x-www-form-urlencoded:
     *              schema:
     *                  $ref: '#definitions/Venue'
     *          application/json:
     *              schema:
     *                  $ref: '#definitions/Venue'
     *     responses:
     *       201:
     *         description: Sukses
     *       400:
     *         description: Bad Request
     *       401:
     *         description: Unauthorized
     *       404:
     *         description: Data tidak ditemukan
     *       422:
     *          description: Unprocessable Entity
     */
    public async update ({ request, response, params }: HttpContextContract){
        try {
            await request.validate(UpdateVenueValidator)
            let data = await Venue.findOrFail(params.id)
            data.name = request.input('name')
            data.address = request.input('address')
            data.phone = request.input('phone')
            data.save()
            response.created({
                status: 'Sukses',
                data: data
            })
        } catch (error) {
            response.badRequest({
                errors: error.messages
            }) 
        }
    }

    public async destroy ({ response, params }: HttpContextContract){
        let data = await Venue.findOrFail(params.id)
        data.delete()
        response.ok({
            status: 'Data venue berhasil dihapus'
        })
    }
}
