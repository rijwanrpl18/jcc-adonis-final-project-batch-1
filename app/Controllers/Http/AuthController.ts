import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import User from 'App/Models/User';
import UserValidator from "App/Validators/UserValidator";
import { schema } from '@ioc:Adonis/Core/Validator'
import Mail from '@ioc:Adonis/Addons/Mail';
import Database from '@ioc:Adonis/Lucid/Database';

export default class AuthController {
    /**
     * @swagger
     * /api/v1/register:
     *   post:
     *     tags:
     *       - Auth
     *     summary: Registrasi user baru
     *     requestBody:
     *       required: true
     *       content:
     *          application/x-www-form-urlencoded:
     *              schema:
     *                  $ref: '#definitions/Register'
     *          application/json:
     *              schema:
     *                  $ref: '#definitions/Register'
     *     responses:
     *       201:
     *         description: Registrasi Berhasil
     *         example:
     *           message: Silakan lakukan verifikasi otp yang dikirim ke email
     *       422:
     *          description: Unprocessable Entity
     */
    public async register({ request, response}: HttpContextContract){
        try {
            const data = await request.validate(UserValidator)
            
            const newUser = await User.create(data)

            let otp_code: number = Math.floor(100000 + Math.random() * 900000)

            await Database.table('otp_codes').insert({
                otp_code: otp_code,
                user_id: newUser.id
            })

            await Mail.send((message) => {
                message
                    .from("admin@sanberdev.com")
                    .to(data.email)
                    .subject('Selamat Datang')
                    .htmlView('mail/otp_verification', {
                        otp_code: otp_code,
                        name: data.name
                    })
            })

            return response.created({
                status: 'Registrasi Berhasil!',
                data: newUser,
                message: 'Silakan lakukan verifikasi otp yang dikirim ke email'
            })
        } catch (error) {
            return response.unprocessableEntity({
                message: error
            })
        }
    }

    /**
     * @swagger
     * /api/v1/login:
     *   post:
     *     tags:
     *       - Auth
     *     summary: Login user
     *     requestBody:
     *       required: true
     *       content:
     *          application/x-www-form-urlencoded:
     *              schema:
     *                  $ref: '#definitions/Login'
     *          application/json:
     *              schema:
     *                  $ref: '#definitions/Login'
     *     responses:
     *       200:
     *         description: Login berhasil
     *       400:
     *          description: Bad Request
     *       422:
     *          description: Unprocessable Entity
     */
    public async login({request, response, auth}: HttpContextContract){
        try {
            const userSchema = schema.create({
                email: schema.string(),
                password: schema.string()
            })
            await request.validate({
                schema: userSchema
            })
            const email = request.input('email')
            const password = request.input('password')
            const token = await auth.use('api').attempt(email, password, {
                expiresIn: '7days'
            })
            return response.ok({
                message: "Login berhasil",
                token
            })
        } catch (error) {
            if (error.guard) {
                return response.badRequest({
                    message: error.message
                })
            } else {
                return response.badRequest({
                    message: error.messages
                })
            }
        }
    }

    /**
     * @swagger
     * /api/v1/otp-confirmation:
     *   post:
     *     tags:
     *       - Auth
     *     summary: Memverifikasi user baru
     *     requestBody:
     *       required: true
     *       content:
     *          application/x-www-form-urlencoded:
     *              schema:
     *                  $ref: '#definitions/OTP'
     *          application/json:
     *              schema:
     *                  $ref: '#definitions/OTP'
     *     responses:
     *       200:
     *         description: Verifikasi berhasil
     *       400:
     *          description: Verifikasi gagal
     *       404:
     *          description: Data tidak ditemukan
     *       422:
     *          description: Unprocessable Entity
     */
    public async otp_verification({request, response}: HttpContextContract){
        const otp_code = request.input('otp_code')
        const email = request.input('email')
        const user = await User.findByOrFail('email', email)
        const data = await Database.from('otp_codes').where('otp_code', otp_code).firstOrFail()
        if(user.id == data.user_id){
            user.isVerified = true
            await user.save()
            return response.ok({
                status: 'success',
                message: 'Verifikasi berhasil'
            })
        } else {
            return response.badRequest({
                status: 'failed',
                message: 'Verifikasi gagal'
            })
        }
    }
}
