import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
// import Database from '@ioc:Adonis/Lucid/Database'
import Field from 'App/Models/Field'
import Venue from 'App/Models/Venue'
import CreateFieldValidator from 'App/Validators/CreateFieldValidator'
import UpdateFieldValidator from 'App/Validators/UpdateFieldValidator'

export default class FieldsController {
    /**
     * @swagger
     * /api/v1/venues/{venue_id}/fields:
     *   get:
     *     security:
     *       - bearerAuth: []
     *     tags:
     *       - Fields
     *     summary: List Data Field
     *     parameters: 
     *       - in: path
     *         name: venue_id
     *         required: true
     *     responses:
     *       200:
     *         description: Sukses
     *       401:
     *         description: Unauthorized
     */
	public async index ({response, params, request}: HttpContextContract) {
        if(request.qs().name){
            let name = request.qs().name
            let data = await Field.query()
                .where('venue_id', params.venue_id)
                .where('name', name)
                .first()
            return response.status(200).json({
                status: 'Sukses',
                data: data
            })
        }
        
        let data = await Field.query().where('venue_id', params.venue_id)
        response.status(200).json({
            status: 'Sukses',
            data: data
        })
	}

    /**
     * @swagger
     * /api/v1/venues/{venue_id}/fields/{id}:
     *   get:
     *     security:
     *       - bearerAuth: []
     *     tags:
     *       - Fields
     *     summary: Get Data Field by ID
     *     parameters: 
     *       - in: path
     *         name: venue_id
     *         required: true
     *       - in: path
     *         name: id
     *         required: true
     *     responses:
     *       200:
     *         description: Berhasil get data field by id
     *       404:
     *         description: Data tidak ditemukan
     *       401:
     *         description: Unauthorized
     */
	public async show ({response, params}: HttpContextContract) {
        // Query Builder
		// let data = await Database
        //     .from('fields')
        //     .select('*')
        //     .where('id', params.id)
        //     .first()

        // ORM
        // let data = await Field.find(params.id)

        // With relationship
        const data = await Field.query()
            .where('id', params.id)
            .preload('bookings', (bookingQuery) => {
                bookingQuery.select(['title', 'play_date_start', 'play_date_end'])
            })
            .firstOrFail()
        response.status(200).json({
            status: 'Sukses',
            data: data
        })
	}

    /**
     * @swagger
     * /api/v1/venues/{venue_id}/fields:
     *   post:
     *     security:
     *       - bearerAuth: []
     *     tags:
     *       - Fields
     *     summary: Store Data Field
     *     parameters: 
     *       - in: path
     *         name: venue_id
     *         required: true
     *     requestBody:
     *       required: true
     *       content:
     *          application/x-www-form-urlencoded:
     *              schema:
     *                  $ref: '#definitions/Field'
     *          application/json:
     *              schema:
     *                  $ref: '#definitions/Field'
     *     responses:
     *       201:
     *         description: Sukses
     *       400:
     *         description: Bad Request
     *       401:
     *         description: Unauthorized
     *       422:
     *          description: Unprocessable Entity
     */
	public async store ({request, response, params}: HttpContextContract) {
		try {
            let field = await request.validate(CreateFieldValidator)
            // create dengan relationship
            let venue = await Venue.findByOrFail('id', params.venue_id)
            const newField = new Field()
            newField.name = field.name
            newField.type = field.type

            await newField.related('venue').associate(venue)
            
            // venue?.related('fields').save(newField)
            
            response.created({
                status: 'Data lapangan berhasil disimpan',
                data: newField
            })
        } catch (error) {
            response.badRequest({
                errors: error.message
            }) 
        }
	}

	public async update ({request, response, params}: HttpContextContract) {
		try {
            await request.validate(UpdateFieldValidator)
            // Query Builder
            // field.venue_id = params.venue_id
            // await Database
            //     .from('fields')
            //     .select('*')
            //     .where('id', params.id)
            //     .update(field)

            // ORM
            let data = await Field.findOrFail(params.id)
            data.name = request.input('name')
            data.type = request.input('type')
            data.venueId = params.venue_id
            data.save()
            response.ok({
                status: 'Data lapangan berhasil diperbaharui'
            })
        } catch (error) {
            response.badRequest({
                errors: error.messages
            }) 
        }
	}

	public async destroy ({response, params}: HttpContextContract) {
        // Query Builder
		// await Database
        //     .from('fields')
        //     .where('id', params.id)
        //     .delete()

        // ORM
        let data = await Field.findOrFail(params.id)
        data.delete()
        response.ok({
            status: 'Data lapangan berhasil dihapus'
        })
	}
}
