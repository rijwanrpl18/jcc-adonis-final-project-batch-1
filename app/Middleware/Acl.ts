import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'

export default class Acl {
	public async handle (
		{auth, response}: HttpContextContract, 
		next: () => Promise<void>,
		roles?: string[]
	) {
		const role = auth.user?.role 
		if(roles?.find(element => element == role) == undefined){
			return response.unauthorized({
				message: "You can't access this endpoint"
			})
		}
		// code for middleware goes here. ABOVE THE NEXT CALL
		await next()
	}
}
